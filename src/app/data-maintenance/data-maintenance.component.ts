import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { DataService } from '../data.service';

@Component({
  selector: 'app-data-maintenance',
  templateUrl: './data-maintenance.component.html',
  styleUrls: ['./data-maintenance.component.scss']
})
export class DataMaintenanceComponent implements OnInit {
  columnsToDisplay: any;
  dataSource = new MatTableDataSource();
  tableData: any;
  // tableData: any = [
  //   { position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H' },
  //   { position: 2, name: 'Helium', weight: 4.0026, symbol: 'He' },
  //   { position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li' },
  //   { position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be' },
  //   { position: 5, name: 'Boron', weight: 10.811, symbol: 'B' },
  //   { position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C' },
  //   { position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N' },
  //   { position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O' },
  //   { position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F' },
  //   { position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne' },
  // ];



  constructor(private dataService: DataService, private router: Router) { }

  ngOnInit(): void {
    this.tableData = this.dataService.data;
    this.columnsToDisplay = Object.keys(this.tableData[0]);
    this.dataSource.data = this.tableData;

  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  navigateToHome() {
    this.router.navigate(['ganttWithoutData']);
  }
}
