import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GanttchartComponent } from './ganttchart/ganttchart.component';
import { HomeComponent } from './home/home.component';
import { MaterialModule } from './material/material.module';
import { SampleComponent } from './sample/sample.component';
import { APIInterceptor } from './api-interceptor';
import { DataMaintenanceComponent } from './data-maintenance/data-maintenance.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { GanttWithouDataComponent } from './gantt-withou-data/gantt-withou-data.component';
import { SidenavComponent } from './sidenav/sidenav.component';

@NgModule({
  declarations: [
    AppComponent,
    GanttchartComponent,
    HomeComponent,
    SampleComponent,
    DataMaintenanceComponent,
    ToolbarComponent,
    GanttWithouDataComponent,
    SidenavComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: APIInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
