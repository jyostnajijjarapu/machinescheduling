import * as d3 from 'd3';

export class GanttChart {

    _state: WeakMap<GanttChart, GanttChartOptions>;

    constructor(options: GanttChartOptions) {
        this._state = new WeakMap();

        this.calculateHeightAndWidth(options);

        let state: GanttChartOptions = this.getState();

        let root = d3.select(`${options.container}`);
        let svg = root.append('svg')
            .attr('width', options.svgWidth)
            .attr('height', options.svgHeight)


        let focus = svg.append("g")
            .attr('class', 'focus')
            .attr("transform", `translate(${options.margin['left']}, ${options.margin['top']})`);

        let focusYAxis = focus.append('g').attr('class', 'axis y-axis');
        let focusXAxis = focus.append('g').attr('class', 'axis x-axis')
            .attr('transform', `translate(0, ${state.height})`);


        let context = svg.append("g")
            .attr('class', 'context')
            .attr("transform", `translate(${options.margin2['left']}, ${options.margin2['top']})`);

        let contextXAxis = context.append('g').attr('class', 'axis x-axis')
            .attr('transform', `translate(0, ${state.height2})`)

        let defs = svg.append("defs").append("clipPath")
            .attr("id", "clip")
            .append("rect")
            .attr("width", state.width)
            .attr("height", state.svgHeight)


        let color = d3.scaleOrdinal(d3.schemeCategory10);
        d3.select('#ganttchart').append('div').attr('id', 'tooltip').attr('style', 'position:fixed; opacity:0;')
            .style("background-color", "pink").style("border", "solid").style("border-width", "2px").style("width", "150px")
            .style("border-radius", "5px").style("padding", "2px").style("font-size", "11px");

        Object.assign(options, {
            root,
            svg,
            focus,
            focusXAxis,
            focusYAxis,
            context,
            contextXAxis,
            defs,
            color
        });

        this.setState(options);
        //console.debug('constructor :: ', this.getState());
    }

    private calculateHeightAndWidth(options: GanttChartOptions) {
        let width = +options.svgWidth - options.margin['left'] - options.margin['right'];
        let height = +options.svgHeight - options.margin['top'] - options.margin['bottom'];
        let height2 = +options.svgHeight - options.margin2['top'] - options.margin2['bottom'];

        Object.assign(options, { width, height, height2 });
        this.setState(options);
        //console.debug('calculateHeightAndWidth :: ', this.getState());
    }



    private setData(data = this.getState().data) {
        let state: GanttChartOptions = this.getState();

        state.data = data;
        state.tasks = state.svg.selectAll('.task')
            .data(data, (d: any) => `${d.Machine}~${d.name}`);
        this.setState(state);
        return this;
    }


    private axisDraw() {
        let state = this.getState();
        let minval: any = d3.min(state.data.map((d: any) => { return new Date(d.Start) }));
        let maxval: any = d3.max(state.data.map((d: any) => { return new Date(d.End) }));
        let x = d3.scaleTime()
            .domain([minval, maxval])
            .range([0, state.width]);

        let y = d3.scaleBand()
            .domain(state.data.map((d: any) => d.Machine))
            .rangeRound([
                0,
                state.height
            ]).padding(state.barSpacing);

        // let x2 = d3.scaleTime()
        //     .domain([minval, maxval]).range([0, state.width]);

        // let y2 = d3.scaleBand()
        //     .domain(state.data.map((d: any) => d.Machine))
        //     .rangeRound([
        //         0,
        //         state.height2
        //     ]).padding(state.barSpacing);
        let format: any = d3.timeFormat('%m-%d-%Y')
        var xAxis = d3.axisBottom(x)
            .tickFormat(format);

        let yAxis = d3.axisLeft(y);
        // let xAxis2 = d3.axisBottom(x2).tickFormat(format);



        Object.assign(state, {
            xScale: x,
            yScale: y,
            // xScale2: x2,
            // yScale2: y2,
            yAxisDraw: yAxis,
            xAxisDraw: xAxis,
            // xAxisDraw2: xAxis2,
            focusXAxis: state.focusXAxis.call(xAxis),
            focusYAxis: state.focusYAxis.call(yAxis),
            // contextXAxis: state.contextXAxis.call(xAxis2)
        });

        this.setState(state);

        //console.debug('Axis :: ', this.getState());
        return this;
    }


    // private tasksEnter() {

    //     let state = this.getState();

    //     let focusTaskGroup = state.focus
    //         .append('g')
    //         .attr('class', 'task-group');

    //     let focusTasks = focusTaskGroup
    //         .selectAll('.task')
    //         .data(state.data)
    //         .enter().append('g').attr('class', 'task')

    //     focusTasks.append('rect')
    //         .attr('class', 'bar')
    //         .attr('x', (d: any) => state.xScale(new Date(d.Start)))
    //         .attr('y', (d: any) => state.yScale(d.Machine))
    //         .attr('fill', (d: any, i: any) => state.color(i))
    //         .attr('width', 0)
    //         .attr('height', (d: any) => state.yScale.bandwidth() / 2);


    //     let contextTaskGroup = state.context
    //         .append('g')
    //         .attr('class', 'task-group');

    //     let contextTask = contextTaskGroup
    //         .selectAll('.task')
    //         .data(state.data)
    //         .enter().append('g').attr('class', 'task');

    //     contextTask.append('rect')
    //         .attr('class', 'bar')
    //         .attr('x', (d: any) => state.xScale2(new Date(d.Start)))
    //         .attr('y', (d: any) => state.yScale2(d.Machine))
    //         .attr('fill', (d: any, i: any) => state.color(i))
    //         .attr('width', 0)
    //         .attr('height', (d: any) => state.yScale2.bandwidth() / 2)


    //     Object.assign(state, {
    //         focusTasks,
    //         contextTask
    //     })

    //     this.setState(state);
    //     //console.debug('task add:: ', this.getState());

    //     return this;

    // }


    // private tasksUpdate() {

    //     let state = this.getState();

    //     let contextTask = state.contextTask;
    //     let focusTask = state.focusTasks;

    //     let contextTaskUpdate = contextTask.selectAll('.bar')
    //         .attr('x', (d: any) => state.xScale2(new Date(d.Start)))
    //         .attr('y', (d: any) => state.yScale2(d.Machine))
    //         //.attr('fill', (d, i) => state.color(i))
    //         .attr('width', (d: any) => state.taskWidth(d))
    //         .attr('height', (d: any) => state.yScale2.bandwidth() / 2)
    //         .attr('transform', `translate(${state.yScale2.range()[0]}, ${state.yScale2.bandwidth() / 4})`)



    //     let focusTaskUpdate = state.focusTasks.selectAll('.bar')
    //         .attr('x', (d: any) => state.xScale(new Date(d.Start)))
    //         .attr('y', (d: any) => state.yScale(d.Machine))
    //         //.attr('fill', (d, i) => state.color(i))
    //         // .data(state.data)
    //         .attr('width', (d: any) => state.taskWidth(d))
    //         .attr('height', (d: any) => state.yScale.bandwidth() / 2)
    //         .attr('transform', `translate(${state.yScale.range()[0]}, ${state.yScale.bandwidth() / 4})`)
    //         .on('mouseover', function () {
    //             d3.select('#tooltip')
    //                 .style('opacity', 1)
    //         })
    //         .on('mouseout', function () {
    //             d3.select('#tooltip').style('opacity', 0)
    //         })
    //         .on('mousemove', function (event: any, d: any) {
    //             d3.select('#tooltip').html(`Start:${d.Start}<br>End:${d.End}<br>Machine:${d.Machine}<br>Planned Order:${d['Planned Order']}`).style('left', (event.pageX) + 'px').style('top', (event.pageY) + 'px')
    //         })

    //     Object.assign(state, {
    //         focusTaskUpdate,
    //         contextTaskUpdate
    //     });

    //     this.setState(state);
    //     //console.debug('task update:: ', this.getState());

    //     return this;

    // }

    // // private updateColor() {
    // //     let state = this.getState();
    // //     state.focusTasks.selectAll('.task-group').attr('fill', (d: any, i: any) => state.color(i));
    // //     state.contextTask.selectAll('.task-group').attr('fill', (d: any, i: any) => state.color(i));
    // // }

    // private attachBrush() {

    //     let state: any = this.getState();

    //     state.brush = d3.brushX()
    //         .extent([[0, 0], [state.width, state.height2 - 2]])
    //         .on("brush end", this.brushed.bind(this));
    //     state.context.append("g")
    //         .attr("class", "brush")
    //         .call(state.brush)
    //         .call(state.brush.move, state.xScale.range());

    //     this.setState(state);
    //     //console.debug('Attach Brush :: ', this.getState());
    //     return this;
    // }
    // // let zoom = d3.zoom()
    // // .on('zoom', handleZoom);

    // private handleZoom(e: any) {
    //     // let state: any = this.getState();
    //     // state.xScale.call(d3.axisBottom(e.transform.rescaleX(state.xScale)))
    //     // state.yScale.call(d3.axisLeft(e.transform.rescaleY(state.yScale)))

    //     d3.select('svg')
    //         .attr('transform', e.transform)

    // }


    // private attachZoom() {
    //     let state: any = this.getState();
    //     state.zoom = d3.zoom()
    //         .scaleExtent([1, 10])
    //         .translateExtent([[0, 0], [state.width, state.height]])
    //         .extent([[0, 0], [state.width, state.height]])
    //         .on('zoom', this.handleZoom);
    //     d3.select('svg')
    //         .call(state.zoom)
    //     // .scaleExtent([1, Infinity])
    //     // .translateExtent([[0, 0], [state.width, state.height]])
    //     // .extent([[0, 0], [state.width, state.height]])
    //     // .on('zoom', function (event) {
    //     //     d3.select('.zoom').attr('transform', event.transform);
    //     // })
    //     // .on("zoom", this.zoomed.bind(this))


    //     // state.svg.append("rect")
    //     //     .attr("class", "zoom")
    //     //     .attr("width", state.width)
    //     //     .attr("height", state.height)
    //     //     // .attr("fill", "transparent")
    //     //     // .style("opacity", "1")
    //     //     .attr("fill", "none")

    //     //     .attr("transform", `translate(${state.margin.left}, ${state.margin.top})`)

    //     // .call(state.zoom)



    //     this.setState(state);
    //     //console.debug('Attach Zoom :: ', this.getState());
    //     return this;
    // }



    // brushed(event: any) {
    //     let state: any = this.getState();
    //     if (event.sourceEvent && event.sourceEvent.type === "zoom") return; // ignore brush-by-zoom
    //     var s = event.selection || state.xScale2.range();
    //     state.xScale.domain(s.map(state.xScale2.invert, state.xScale2));
    //     state.focusXAxis.call(state.xAxisDraw);

    //     this.redrawFocused();

    //     state.svg.select(".zoom").call(state.zoom.transform, d3.zoomIdentity
    //         .scale(state.width / (s[1] - s[0]))
    //         .translate(-s[0], 0));
    // }

    // zoomed(event: any) {
    //     // console.log(event, "eeeeee")
    //     let state = this.getState();

    //     if (event.sourceEvent && event.sourceEvent.type === "brush") return; // ignore zoom-by-brush
    //     var t = event.transform;
    //     state.xScale.domain(t.rescaleX(state.xScale2).domain());

    //     this.redrawFocused();

    //     state.focusXAxis.call(state.xAxisDraw);
    //     state.context.select(".brush").call(state.brush.move, state.xScale.range().map(t.invertX, t));
    // }



    // // private tasksExit() {
    // //     let state = this.getState();

    // //     let contextTask = state.contextTask;
    // //     let focusTask = state.focusTasks;
    // //     let focusTaskExit = focusTask.selectAll('.bar')
    // //         .data(state.data)
    // //         .exit()
    // //         //.attr('width', 0)
    // //         .remove();
    // //     let contextTaskExit = contextTask.selectAll('.bar')
    // //         .data(state.data)
    // //         .exit()
    // //         //.attr('width', 0)
    // //         .remove();

    // //     Object.assign(state, {
    // //         focusTaskExit,
    // //         contextTaskExit
    // //     });

    // //     this.setState(state);
    // //     //console.debug('task update:: ', this.getState());

    // //     return this;
    // // }

    // private redrawFocused() {
    //     let state = this.getState();
    //     let focusTask = state.focusTasks;

    //     let tasks = focusTask.select('.task-group').selectAll('.task');

    //     let focusTaskUpdate = focusTask.selectAll('.bar')
    //         .attr('x', (d: any) => state.xScale(new Date(d.Start)))

    //         .attr('x', (d: any) => {
    //             console.log(state.xScale(new Date(d.Start)))
    //             if (state.xScale(new Date(d.Start)) < 0) {
    //                 // d3.select('.task').style("z-index", 0)
    //                 console.log(state.xScale(new Date(d.Start)) - (state.xScale(new Date(d.Start))))
    //                 console.log(state.xScale(new Date(d.Start)) - 80)
    //                 // return state.xScale(new Date(d.Start)) + (state.xScale(new Date(d.Start))) + (state.xScale(new Date(d.Start)))
    //                 return state.xScale(new Date(d.Start)) - state.xScale(new Date(d.Start));
    //                 // return state.xScale(new Date(d.Start)) - 80;
    //             }
    //             else {
    //                 return state.xScale(new Date(d.Start));
    //             }
    //         })

    //         // .attr('x', 0.5)

    //         .attr('y', (d: any) => state.yScale(d.Machine))
    //         .attr('width', (d: any) => state.taskWidth(d))
    //         .attr('height', (d: any) => state.yScale.bandwidth() / 2)
    //         .attr('transform', `translate(${state.yScale.range()[0]}, ${state.yScale.bandwidth() / 4})`);

    //     Object.assign(state, {
    //         focusTaskUpdate
    //     });

    //     this.setState(state);
    //     //console.debug('redrawFocused:: ', this.getState());

    //     return this;
    // }

    build(data = this.getState().data) {

        let self = this,
            state = this.getState();

        this.setData(data)
            .axisDraw()
        // .tasksEnter()
        // .tasksUpdate()
        // .attachZoom()
        // .attachBrush();

        //console.debug('Build', self.getState(), data);

        return self;
    }



    getState() {
        let self = this;
        return this._state.get(self) || new GanttChartOptions();
    }

    setState(newState: GanttChartOptions) {
        let currentState = this.getState();
        this._state.set(this, Object.assign(currentState, newState));
        return this;
    }

}


export class GanttChartOptions implements IGanttChartOptions {

    container: string;
    data: any;
    svgWidth: number | string;
    svgHeight: number | string;
    margin: { [key: string]: number };
    margin2: { [key: string]: number };
    barSpacing: any;
    width: any;
    height?: any;
    height2?: any;
    focus?: any;
    context?: any;

    root?: any;
    svg?: any;
    tasks?: any;
    focusTasks?: any;
    contextTask?: any;
    contextTaskUpdate?: any;
    focusTaskUpdate?: any;
    focusTaskExit?: any;
    contextTaskExit?: any;


    color?: any;
    xScale?: any;
    yScale?: any;
    xScale2?: any;
    yScale2?: any;
    yAxisDraw?: any;
    xAxisDraw?: any;
    xAxisDraw2?: any;
    focusXAxis?: any;
    focusYAxis?: any;
    contextXAxis?: any;
    defs?: any;
    brush?: any;
    zoom?: any;

    constructor(
        container: string = '#chart', data: any = [], svgHeight: number = 0, svgWidth: number = 0,
        margin: any = {}, margin2: any = {}, barSpacing: number = 0.25
    ) {
        this.container = container;
        this.data = data;
        this.svgHeight = svgHeight;
        this.svgWidth = svgWidth;
        this.margin = margin;
        this.margin2 = margin2;
        this.barSpacing = barSpacing;
    }

    taskWidth(d: any) {
        return this.xScale(new Date(d.End)) - this.xScale(new Date(d.Start));
    }

}

export interface IGanttChartOptions {

    container: string;
    data: any;
    svgWidth: number | string;
    svgHeight: number | string;
    margin: { [key: string]: number };
    margin2: { [key: string]: number };
    barSpacing: any;
    root?: any;
    svg?: any;
    width?: number;
    height?: any;
    height2?: any;
    focus?: any;
    context?: any;


    tasks?: any;
    focusTasks?: any;
    contextTask?: any;
    contextTaskUpdate?: any;
    focusTaskUpdate?: any;
    focusTaskExit?: any;
    contextTaskExit?: any;

    xScale?: any;
    yScale?: any;
    xScale2?: any;
    yScale2?: any;
    yAxisDraw?: any;
    xAxisDraw?: any;
    xAxisDraw2?: any;
    focusXAxis?: any;
    focusYAxis?: any;
    contextXAxis?: any;
    color?: any;
    defs?: any;
    brush?: any;
    zoom?: any;

}