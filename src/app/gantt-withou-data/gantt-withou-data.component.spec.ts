import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GanttWithouDataComponent } from './gantt-withou-data.component';

describe('GanttWithouDataComponent', () => {
  let component: GanttWithouDataComponent;
  let fixture: ComponentFixture<GanttWithouDataComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GanttWithouDataComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GanttWithouDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
