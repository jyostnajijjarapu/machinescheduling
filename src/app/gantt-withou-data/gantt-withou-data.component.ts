import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { GanttChart, GanttChartOptions } from './gantt-withou-data'

@Component({
  selector: 'app-gantt-withou-data',
  templateUrl: './gantt-withou-data.component.html',
  styleUrls: ['./gantt-withou-data.component.scss']
})
export class GanttWithouDataComponent implements OnInit {

  svgWidth = 960;
  svgHeight = 500;
  margin = { top: 20, right: 20, bottom: 110, left: 40 };
  margin2 = { top: 430, right: 20, bottom: 30, left: 40 };

  constructor(private dataService: DataService) { }

  ngOnInit() {

    this.dataService.getData().subscribe((data: any) => {

      let options = new GanttChartOptions(
        '#ganttchart',
        data,
        this.svgHeight,
        this.svgWidth,
        this.margin,
        this.margin2
      );

      const gc = new GanttChart(options).build();
    });

  }

}
