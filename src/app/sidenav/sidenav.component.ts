import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {
  orders: any = [];
  machines: any = [];
  constructor(private dataservice: DataService) { }

  ngOnInit(): void {
    console.log(this.dataservice.data[0]['Machine'])
    for (let i = 0; i < this.dataservice.data.length; i++) {

      this.orders.push(this.dataservice.data[i]['Planned Order']);
      this.machines.push(this.dataservice.data[i]['Machine']);
      this.machines = [...new Set(this.machines)]
    }
    console.log(this.orders, this.machines)
  }

}
