import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DataMaintenanceComponent } from './data-maintenance/data-maintenance.component';
import { GanttWithouDataComponent } from './gantt-withou-data/gantt-withou-data.component';
import { GanttchartComponent } from './ganttchart/ganttchart.component';
import { HomeComponent } from './home/home.component';
import { SampleComponent } from './sample/sample.component';
import { ToolbarComponent } from './toolbar/toolbar.component';

const routes: Routes = [

  { path: '', component: HomeComponent, pathMatch: "full" },
  { path: 'home', component: HomeComponent },

  { path: 'tool', component: ToolbarComponent },
  { path: 'data', component: DataMaintenanceComponent },
  { path: 'gantt', component: GanttchartComponent },
  { path: "ganttWithoutData", component: GanttWithouDataComponent }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
