import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSelectModule } from '@angular/material/select';
import { MatMenuModule } from '@angular/material/menu';
@NgModule({
  declarations: [],
  imports: [
    MatSidenavModule,
    MatIconModule,
    MatCardModule, MatDialogModule, MatTableModule, MatFormFieldModule, MatInputModule, MatButtonModule, MatToolbarModule, MatSelectModule, MatMenuModule,
    CommonModule
  ],
  exports: [
    MatSidenavModule,
    MatIconModule,
    MatCardModule, MatDialogModule, MatTableModule, MatFormFieldModule, MatInputModule, MatButtonModule, MatToolbarModule, MatSelectModule, MatMenuModule
  ]
})
export class MaterialModule { }
