import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  // data: any;
  constructor(private httpService: HttpService) { }
  getData(): Observable<any> {
    return of<any>(this.data);
  }
  data3: any = [
    {
      "name": "Óscar",
      "category": "Óscar",
      "company": "TCS",
      "from": "02/03/2017 08:00:00",
      "to": "02/03/2017 12:30:00",
      "comment": "Things never end"
    },
    {
      "name": "Óscar",
      "category": "Óscar",
      "company": "TCS",
      "from": "02/03/2017 13:00:00",
      "to": "02/03/2017 17:30:00",
      "comment": "Things never end"
    },
    {
      "name": "Peter",
      "category": "Peter",
      "company": "DAN",
      "from": "02/03/2017 12:30:00",
      "to": "02/03/2017 18:00:00",
      "comment": "Even more stuff"
    },
    {
      "name": "Johnny",
      "category": "Johnny",
      "company": "Infosys",
      "from": "02/03/2017 17:30:00",
      "to": "02/03/2017 21:00:00",
      "comment": "Agggh"
    }

  ];
  data2: any = [
    { "Machine": 1101.0, "Planned Order": "job_2_task_0", "Start": "2020-01-01", "End": "2020-01-03" },
    { "Machine": 1101.0, "Planned Order": "job_3_task_0", "Start": "2020-01-03", "End": "2020-01-06" },
    { "Machine": 1101.0, "Planned Order": "job_4_task_0", "Start": "2020-01-06", "End": "2020-01-08" },
    { "Machine": 1101.0, "Planned Order": "job_5_task_0", "Start": "2020-01-08", "End": "2020-01-11" },
    { "Machine": 1101.0, "Planned Order": "job_6_task_0", "Start": "2020-01-11", "End": "2020-01-13" },
    { "Machine": 1101.0, "Planned Order": "job_7_task_0", "Start": "2020-01-13", "End": "2020-01-16" },
    { "Machine": 1101.0, "Planned Order": "job_8_task_0", "Start": "2020-01-16", "End": "2020-01-18" },
    { "Machine": 1101.0, "Planned Order": "job_9_task_0", "Start": "2020-01-18", "End": "2020-01-21" },
    { "Machine": 1101.0, "Planned Order": "job_10_task_0", "Start": "2020-01-21", "End": "2020-01-23" },
    { "Machine": 1101.0, "Planned Order": "job_11_task_0", "Start": "2020-01-23", "End": "2020-01-26" },
    { "Machine": 1101.0, "Planned Order": "job_12_task_0", "Start": "2020-01-26", "End": "2020-01-28" },
    { "Machine": 1101.0, "Planned Order": "job_13_task_0", "Start": "2020-01-28", "End": "2020-01-31" },
    { "Machine": 1101.0, "Planned Order": "job_14_task_0", "Start": "2020-01-31", "End": "2020-02-02" },
    { "Machine": 1101.0, "Planned Order": "job_15_task_0", "Start": "2020-02-02", "End": "2020-02-05" },
    { "Machine": 1101.0, "Planned Order": "job_16_task_0", "Start": "2020-02-05", "End": "2020-02-07" },
    { "Machine": 1101.0, "Planned Order": "job_17_task_0", "Start": "2020-02-07", "End": "2020-02-10" },
    { "Machine": 1101.0, "Planned Order": "job_18_task_0", "Start": "2020-02-10", "End": "2020-02-12" },
    { "Machine": 1101.0, "Planned Order": "job_19_task_0", "Start": "2020-02-12", "End": "2020-02-15" },
    { "Machine": 1101.0, "Planned Order": "job_20_task_0", "Start": "2020-02-15", "End": "2020-02-17" },
    { "Machine": 1101.0, "Planned Order": "job_21_task_0", "Start": "2020-02-17", "End": "2020-02-20" },
    { "Machine": 1101.0, "Planned Order": "job_22_task_0", "Start": "2020-02-20", "End": "2020-02-22" },
    { "Machine": 1101.0, "Planned Order": "job_23_task_0", "Start": "2020-02-22", "End": "2020-02-25" },
    { "Machine": 1101.0, "Planned Order": "job_24_task_0", "Start": "2020-02-25", "End": "2020-02-27" },
    { "Machine": 1101.0, "Planned Order": "job_930_task_0", "Start": "2020-02-27", "End": "2020-03-01" },
    { "Machine": 1101.0, "Planned Order": "job_1_task_0", "Start": "2020-03-01", "End": "2020-03-04" },
    { "Machine": 1101.0, "Planned Order": "job_0_task_0", "Start": "2020-03-04", "End": "2020-03-08" },
    { "Machine": 1101.0, "Planned Order": "job_606_task_0", "Start": "2020-03-08", "End": "2020-03-16" },
    { "Machine": 1101.0, "Planned Order": "job_183_task_0", "Start": "2020-03-16", "End": "2020-03-30" },
    { "Machine": 1101.0, "Planned Order": "job_51_task_0", "Start": "2020-03-30", "End": "2020-04-19" },
    { "Machine": 1101.0, "Planned Order": "job_73_task_0", "Start": "2020-04-19", "End": "2020-05-11" },
    { "Machine": 1101.0, "Planned Order": "job_74_task_0", "Start": "2020-05-11", "End": "2020-06-01" },
    { "Machine": 1101.0, "Planned Order": "job_75_task_0", "Start": "2020-06-01", "End": "2020-06-22" },
    { "Machine": 1101.0, "Planned Order": "job_76_task_0", "Start": "2020-06-22", "End": "2020-07-14" },
    { "Machine": 1101.0, "Planned Order": "job_77_task_0", "Start": "2020-07-14", "End": "2020-08-04" },
    { "Machine": 1101.0, "Planned Order": "job_78_task_0", "Start": "2020-08-04", "End": "2020-08-26" },
    { "Machine": 1101.0, "Planned Order": "job_79_task_0", "Start": "2020-08-26", "End": "2020-09-16" },
    { "Machine": 1101.0, "Planned Order": "job_80_task_0", "Start": "2020-09-16", "End": "2020-10-07" },
    { "Machine": 1101.0, "Planned Order": "job_81_task_0", "Start": "2020-10-07", "End": "2020-10-29" },
    { "Machine": 1101.0, "Planned Order": "job_82_task_0", "Start": "2020-10-29", "End": "2020-11-19" },
    { "Machine": 1101.0, "Planned Order": "job_83_task_0", "Start": "2020-11-19", "End": "2020-12-10" },
    { "Machine": 1101.0, "Planned Order": "job_84_task_0", "Start": "2020-12-10", "End": "2021-01-01" },
    { "Machine": 1101.0, "Planned Order": "job_85_task_0", "Start": "2021-01-01", "End": "2021-01-22" },
    { "Machine": 1101.0, "Planned Order": "job_86_task_0", "Start": "2021-01-22", "End": "2021-02-13" },
    { "Machine": 1101.0, "Planned Order": "job_87_task_0", "Start": "2021-02-13", "End": "2021-03-06" },
    { "Machine": 1101.0, "Planned Order": "job_88_task_0", "Start": "2021-03-06", "End": "2021-03-27" },
    { "Machine": 1101.0, "Planned Order": "job_89_task_0", "Start": "2021-03-27", "End": "2021-04-18" },
    { "Machine": 1101.0, "Planned Order": "job_90_task_0", "Start": "2021-04-18", "End": "2021-05-09" },
    { "Machine": 1101.0, "Planned Order": "job_91_task_0", "Start": "2021-05-09", "End": "2021-05-30" },
    { "Machine": 1101.0, "Planned Order": "job_92_task_0", "Start": "2021-05-30", "End": "2021-06-21" },
    { "Machine": 1101.0, "Planned Order": "job_93_task_0", "Start": "2021-06-21", "End": "2021-07-12" },
    { "Machine": 1101.0, "Planned Order": "job_94_task_0", "Start": "2021-07-12", "End": "2021-08-03" },
    { "Machine": 1101.0, "Planned Order": "job_95_task_0", "Start": "2021-08-03", "End": "2021-08-24" },
    { "Machine": 1102.0, "Planned Order": "job_1002_task_0", "Start": "2020-01-01", "End": "2020-01-01" },
    { "Machine": 1102.0, "Planned Order": "job_1004_task_0", "Start": "2020-01-01", "End": "2020-01-01" },
    { "Machine": 1102.0, "Planned Order": "job_1003_task_0", "Start": "2020-01-01", "End": "2020-01-01" },
    { "Machine": 1102.0, "Planned Order": "job_1000_task_0", "Start": "2020-01-01", "End": "2020-01-01" },
    { "Machine": 1102.0, "Planned Order": "job_1001_task_0", "Start": "2020-01-01", "End": "2020-01-01" },
    { "Machine": 1102.0, "Planned Order": "job_998_task_0", "Start": "2020-01-01", "End": "2020-01-01" },
    { "Machine": 1102.0, "Planned Order": "job_996_task_0", "Start": "2020-01-01", "End": "2020-01-02" },
    { "Machine": 1102.0, "Planned Order": "job_999_task_0", "Start": "2020-01-02", "End": "2020-01-02" },
    { "Machine": 1102.0, "Planned Order": "job_997_task_0", "Start": "2020-01-02", "End": "2020-01-03" },
    { "Machine": 1102.0, "Planned Order": "job_995_task_0", "Start": "2020-01-03", "End": "2020-01-03" },
    { "Machine": 1102.0, "Planned Order": "job_994_task_0", "Start": "2020-01-03", "End": "2020-01-04" },
    { "Machine": 1102.0, "Planned Order": "job_992_task_0", "Start": "2020-01-04", "End": "2020-01-05" },
    { "Machine": 1102.0, "Planned Order": "job_990_task_0", "Start": "2020-01-05", "End": "2020-01-06" },
    { "Machine": 1102.0, "Planned Order": "job_987_task_0", "Start": "2020-01-06", "End": "2020-01-07" },
    { "Machine": 1102.0, "Planned Order": "job_993_task_0", "Start": "2020-01-07", "End": "2020-01-08" },
    { "Machine": 1102.0, "Planned Order": "job_989_task_0", "Start": "2020-01-08", "End": "2020-01-10" },
    { "Machine": 1102.0, "Planned Order": "job_988_task_0", "Start": "2020-01-10", "End": "2020-01-12" },
    { "Machine": 1102.0, "Planned Order": "job_957_task_0", "Start": "2020-01-12", "End": "2020-01-13" },
    { "Machine": 1102.0, "Planned Order": "job_991_task_0", "Start": "2020-01-13", "End": "2020-01-15" },
    { "Machine": 1102.0, "Planned Order": "job_958_task_0", "Start": "2020-01-15", "End": "2020-01-17" },
    { "Machine": 1102.0, "Planned Order": "job_984_task_0", "Start": "2020-01-17", "End": "2020-01-19" },
    { "Machine": 1102.0, "Planned Order": "job_922_task_0", "Start": "2020-01-19", "End": "2020-01-21" },
    { "Machine": 1102.0, "Planned Order": "job_950_task_0", "Start": "2020-01-21", "End": "2020-01-23" },
    { "Machine": 1102.0, "Planned Order": "job_960_task_0", "Start": "2020-01-23", "End": "2020-01-26" },
    { "Machine": 1102.0, "Planned Order": "job_961_task_0", "Start": "2020-01-26", "End": "2020-01-28" },
    { "Machine": 1102.0, "Planned Order": "job_962_task_0", "Start": "2020-01-28", "End": "2020-01-30" },
    { "Machine": 1102.0, "Planned Order": "job_963_task_0", "Start": "2020-01-30", "End": "2020-02-02" },
    { "Machine": 1102.0, "Planned Order": "job_964_task_0", "Start": "2020-02-02", "End": "2020-02-04" },
    { "Machine": 1102.0, "Planned Order": "job_965_task_0", "Start": "2020-02-04", "End": "2020-02-07" },
    { "Machine": 1102.0, "Planned Order": "job_966_task_0", "Start": "2020-02-07", "End": "2020-02-09" },
    { "Machine": 1102.0, "Planned Order": "job_967_task_0", "Start": "2020-02-09", "End": "2020-02-12" },
    { "Machine": 1102.0, "Planned Order": "job_968_task_0", "Start": "2020-02-12", "End": "2020-02-14" },
    { "Machine": 1102.0, "Planned Order": "job_969_task_0", "Start": "2020-02-14", "End": "2020-02-16" },
    { "Machine": 1102.0, "Planned Order": "job_970_task_0", "Start": "2020-02-16", "End": "2020-02-19" },
    { "Machine": 1102.0, "Planned Order": "job_971_task_0", "Start": "2020-02-19", "End": "2020-02-21" },
    { "Machine": 1102.0, "Planned Order": "job_972_task_0", "Start": "2020-02-21", "End": "2020-02-24" },
    { "Machine": 1102.0, "Planned Order": "job_973_task_0", "Start": "2020-02-24", "End": "2020-02-26" },
    { "Machine": 1102.0, "Planned Order": "job_974_task_0", "Start": "2020-02-26", "End": "2020-02-28" },
    { "Machine": 1102.0, "Planned Order": "job_975_task_0", "Start": "2020-02-28", "End": "2020-03-02" },
    { "Machine": 1102.0, "Planned Order": "job_976_task_0", "Start": "2020-03-02", "End": "2020-03-04" },
    { "Machine": 1102.0, "Planned Order": "job_977_task_0", "Start": "2020-03-04", "End": "2020-03-07" },
    { "Machine": 1102.0, "Planned Order": "job_978_task_0", "Start": "2020-03-07", "End": "2020-03-09" },
    { "Machine": 1102.0, "Planned Order": "job_979_task_0", "Start": "2020-03-09", "End": "2020-03-12" },
    { "Machine": 1102.0, "Planned Order": "job_980_task_0", "Start": "2020-03-12", "End": "2020-03-14" },
    { "Machine": 1102.0, "Planned Order": "job_981_task_0", "Start": "2020-03-14", "End": "2020-03-16" },
    { "Machine": 1102.0, "Planned Order": "job_982_task_0", "Start": "2020-03-16", "End": "2020-03-19" },
    { "Machine": 1102.0, "Planned Order": "job_983_task_0", "Start": "2020-03-19", "End": "2020-03-21" },
    { "Machine": 1102.0, "Planned Order": "job_945_task_0", "Start": "2020-03-21", "End": "2020-03-24" },
    { "Machine": 1102.0, "Planned Order": "job_926_task_0", "Start": "2020-03-24", "End": "2020-03-27" },
    { "Machine": 1102.0, "Planned Order": "job_953_task_0", "Start": "2020-03-27", "End": "2020-03-29" },
    { "Machine": 1102.0, "Planned Order": "job_925_task_0", "Start": "2020-03-29", "End": "2020-04-01" },
    { "Machine": 1102.0, "Planned Order": "job_951_task_0", "Start": "2020-04-01", "End": "2020-04-04" },
    { "Machine": 1102.0, "Planned Order": "job_948_task_0", "Start": "2020-04-04", "End": "2020-04-07" },
    { "Machine": 1102.0, "Planned Order": "job_985_task_0", "Start": "2020-04-07", "End": "2020-04-10" },
    { "Machine": 1102.0, "Planned Order": "job_986_task_0", "Start": "2020-04-10", "End": "2020-04-12" },
    { "Machine": 1102.0, "Planned Order": "job_952_task_0", "Start": "2020-04-12", "End": "2020-04-15" },
    { "Machine": 1102.0, "Planned Order": "job_946_task_0", "Start": "2020-04-15", "End": "2020-04-19" },
    { "Machine": 1102.0, "Planned Order": "job_928_task_0", "Start": "2020-04-19", "End": "2020-04-22" },
    { "Machine": 1102.0, "Planned Order": "job_849_task_0", "Start": "2020-04-22", "End": "2020-04-25" },
    { "Machine": 1102.0, "Planned Order": "job_900_task_0", "Start": "2020-04-25", "End": "2020-04-28" },
    { "Machine": 1102.0, "Planned Order": "job_944_task_0", "Start": "2020-04-28", "End": "2020-05-01" },
    { "Machine": 1102.0, "Planned Order": "job_903_task_0", "Start": "2020-05-01", "End": "2020-05-05" },
    { "Machine": 1102.0, "Planned Order": "job_923_task_0", "Start": "2020-05-05", "End": "2020-05-08" },
    { "Machine": 1102.0, "Planned Order": "job_934_task_0", "Start": "2020-05-08", "End": "2020-05-11" },
    { "Machine": 1102.0, "Planned Order": "job_949_task_0", "Start": "2020-05-11", "End": "2020-05-14" },
    { "Machine": 1102.0, "Planned Order": "job_954_task_0", "Start": "2020-05-14", "End": "2020-05-18" },
    { "Machine": 1102.0, "Planned Order": "job_877_task_0", "Start": "2020-05-18", "End": "2020-05-21" },
    { "Machine": 1102.0, "Planned Order": "job_875_task_0", "Start": "2020-05-21", "End": "2020-05-24" },
    { "Machine": 1102.0, "Planned Order": "job_931_task_0", "Start": "2020-05-24", "End": "2020-05-28" },
    { "Machine": 1102.0, "Planned Order": "job_959_task_0", "Start": "2020-05-28", "End": "2020-05-31" },
    { "Machine": 1102.0, "Planned Order": "job_921_task_0", "Start": "2020-05-31", "End": "2020-06-03" },
    { "Machine": 1102.0, "Planned Order": "job_901_task_0", "Start": "2020-06-03", "End": "2020-06-07" },
    { "Machine": 1102.0, "Planned Order": "job_878_task_0", "Start": "2020-06-07", "End": "2020-06-10" },
    { "Machine": 1102.0, "Planned Order": "job_927_task_0", "Start": "2020-06-10", "End": "2020-06-14" },
    { "Machine": 1102.0, "Planned Order": "job_902_task_0", "Start": "2020-06-14", "End": "2020-06-18" },
    { "Machine": 1102.0, "Planned Order": "job_943_task_0", "Start": "2020-06-18", "End": "2020-06-21" },
    { "Machine": 1102.0, "Planned Order": "job_826_task_0", "Start": "2020-06-21", "End": "2020-06-25" },
    { "Machine": 1102.0, "Planned Order": "job_827_task_0", "Start": "2020-06-25", "End": "2020-06-29" },
    { "Machine": 1102.0, "Planned Order": "job_828_task_0", "Start": "2020-06-29", "End": "2020-07-02" },
    { "Machine": 1102.0, "Planned Order": "job_829_task_0", "Start": "2020-07-02", "End": "2020-07-06" },
    { "Machine": 1102.0, "Planned Order": "job_830_task_0", "Start": "2020-07-06", "End": "2020-07-10" },];

  data: any = [{ "Machine": 1101.0, "Planned Order": "job_1_task_0", "Start": "2020-01-01", "End": "2020-01-03" }, { "Machine": 1101.0, "Planned Order": "job_0_task_0", "Start": "2020-01-03", "End": "2020-01-06" }, { "Machine": 1102.0, "Planned Order": "job_2_task_0", "Start": "2020-01-01", "End": "2020-01-05" }, { "Machine": 1102.0, "Planned Order": "job_0_task_1", "Start": "2020-01-06", "End": "2020-01-08" }, { "Machine": 1102.0, "Planned Order": "job_1_task_2", "Start": "2020-01-08", "End": "2020-01-12" }, { "Machine": 1103.0, "Planned Order": "job_1_task_1", "Start": "2020-01-03", "End": "2020-01-04" }, { "Machine": 1103.0, "Planned Order": "job_2_task_1", "Start": "2020-01-05", "End": "2020-01-08" }, { "Machine": 1103.0, "Planned Order": "job_0_task_2", "Start": "2020-01-08", "End": "2020-01-10" }]


  // getData(): Observable<any> {
  //   this.httpService.get("master").subscribe((res: any) => {
  //     this.data = res;

  //   })
  //   return of<any>(this.data);
  // }


}
