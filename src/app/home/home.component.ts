import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  constructor(private dialog: MatDialog, private router: Router) { }
  @ViewChild('showDialog', { static: true })
  showDialog!: TemplateRef<any>;

  ngOnInit(): void {
  }


  handleFileSelect(evt: any) {
    console.log(evt, "event")
    var files = evt.target.files; // FileList object
    var file = files[0];
    if (file) {
      this.openDialogpopup();
    }
    console.log("name", file)
    var reader = new FileReader();
    reader.readAsText(file);
    reader.onload = (event: any) => {
      var csv = event.target.result; // Content of CSV file
      console.log(csv, "file")
    }
  }
  openDialogpopup() {
    let dialogRef = this.dialog.open(this.showDialog);
    dialogRef.afterClosed().subscribe((res: any) => {
      if (res) {
        this.router.navigate(["/data"]);
      }


    });
  }
  gotoGanttWithOutData() {
    this.router.navigate(['ganttWithoutData'])
  }
}
