import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { GanttChart, GanttChartOptions } from './ganttchart'
@Component({
  selector: 'app-ganttchart',
  templateUrl: './ganttchart.component.html',
  styleUrls: ['./ganttchart.component.scss']
})
export class GanttchartComponent implements OnInit {
  svgWidth = 960;
  svgHeight = 500;
  margin = { top: 20, right: 20, bottom: 110, left: 40 };
  margin2 = { top: 430, right: 20, bottom: 30, left: 40 };

  constructor(private dataService: DataService) { }

  ngOnInit() {

    this.dataService.getData().subscribe((data: any) => {

      let options = new GanttChartOptions(
        '#chart1',
        data,
        this.svgHeight,
        this.svgWidth,
        this.margin,
        this.margin2
      );

      const gc = new GanttChart(options).build();
    });

  }
}
